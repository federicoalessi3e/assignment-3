package Drop;

import model.*;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;


public class DropDB {
    public void dropDatabase(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        entityManager.createNativeQuery("DELETE IGNORE FROM " + City.TABLE_NAME + " WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM " + User.TABLE_NAME + " WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM " + PersonalTrainer.TABLE_NAME + " WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM " + Gym.TABLE_NAME + " WHERE 1").executeUpdate();


        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
