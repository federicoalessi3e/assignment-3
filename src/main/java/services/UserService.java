package services;

import model.User;
import daos.UtenteDao;

import java.util.List;

public class UserService {
    private UtenteDao userdao;

    public UserService() {
        this.userdao = new UtenteDao();
    }

    public User get(Integer id) {
        userdao.beginTransaction();
        User user = userdao.get(id);
        userdao.commitTransaction();

        return user;
    }

    public List<User> all() {
        userdao.beginTransaction();
        List<User> users = userdao.all();
        userdao.commitTransaction();

        return users;
    }

    public User create(User user) {
        userdao.beginTransaction();
        user = userdao.create(user);
        userdao.commitTransaction();

        return user;
    }

    public void delete(User user){
        List<User> users = this.all();
        userdao.beginTransaction();

        for(User user2: users){
            if(user2.getIdInvito() != null && (user2.getIdInvito().getCodiceFiscale() == user.getCodiceFiscale())){
                user2.setIdInvito(null);
                userdao.update(user2);
            }
        }

        user.setIdInvito(null);
        userdao.update(user);
        userdao.commitTransaction();

        userdao.beginTransaction();
        userdao.delete(user);
        userdao.commitTransaction();
    }

    public User update(User user){
        userdao.beginTransaction();
        user = userdao.update(user);
        userdao.commitTransaction();

        return user;
    }
}
