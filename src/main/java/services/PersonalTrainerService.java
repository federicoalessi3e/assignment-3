package services;

import model.PersonalTrainer;
import model.User;

import daos.PersonalTrainerDao;

import java.util.List;

public class PersonalTrainerService {
    private PersonalTrainerDao trainerdao;


    public PersonalTrainerService() {
        this.trainerdao = new PersonalTrainerDao();
    }

    public PersonalTrainer get(Integer id) {
        trainerdao.beginTransaction();
        PersonalTrainer trainer = trainerdao.get(id);
        trainerdao.commitTransaction();

        return trainer;
    }

    public List<PersonalTrainer> all() {
        trainerdao.beginTransaction();
        List<PersonalTrainer> trainers = trainerdao.all();
        trainerdao.commitTransaction();

        return trainers;
    }

    public PersonalTrainer create(PersonalTrainer trainer) {
        trainerdao.beginTransaction();
        trainer = trainerdao.create(trainer);
        trainerdao.commitTransaction();

        return trainer;
    }

    public void delete(PersonalTrainer trainer){
        UserService userService = new UserService();
        List<User> users = userService.all();

        for(User user: users){
            if(user.getPersonalTrainerId() != null && (user.getPersonalTrainerId().getId() == trainer.getId())){
                user.setPersonalTrainerId(null);
                userService.update(user);
            }
        }

        trainerdao.beginTransaction();
        trainerdao.delete(trainer);
        trainerdao.commitTransaction();
    }

    public PersonalTrainer update(PersonalTrainer trainer){
        trainerdao.beginTransaction();
        trainer = trainerdao.update(trainer);
        trainerdao.commitTransaction();

        return trainer;
    }
}