package model;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name=Gym.TABLE_NAME)
public class Gym {

    public static final String TABLE_NAME = "Gym";

    @Id
    @Column(name = "GYM_ID")
    @GeneratedValue( strategy= GenerationType.AUTO )
    private int id;

    @Column(name = "24_H")
    private boolean isOpen24h;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "CITY_CAP")
    private City city;

    public Gym(){}

    public Gym(boolean open24, City city){
        this.setOpen24h(open24);
        this.setCity(city);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isOpen24h() {
        return isOpen24h;
    }

    public void setOpen24h(boolean open24h) {
        isOpen24h = open24h;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Gym{" +
                "id=" + id +
                ", city=" + city +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
