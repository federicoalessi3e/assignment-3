package model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name=PersonalTrainer.TABLE_NAME)
public class PersonalTrainer {

    public static final String TABLE_NAME = "PersonalTrainer";

    @Id
    @Column(name = "PT_ID")
    @GeneratedValue( strategy= GenerationType.AUTO )
    private int id;

    @Column(name = "Lastname")
    private String cognome;

    @Column(name = "Sex")
    private String sesso;

    @Column(name = "Age")
    private int età;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "Palestra_ID")
    private Gym palestraId;

    public PersonalTrainer(){}

    public PersonalTrainer(String cognome, String sesso, int età, Gym palestra){
        this.setCognome(cognome);
        this.setSesso(sesso);
        this.setEtà(età);
        this.setPalestraId(palestra);
    }


    public int getId() {
        return id;
    }


    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public int getEtà() {
        return età;
    }

    public void setEtà(int età) {
        this.età = età;
    }

    public Gym getPalestraId() {
        return palestraId;
    }

    public void setPalestraId(Gym palestraId) {
        this.palestraId = palestraId;
    }


    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public String toString() {
        return "PersonalTrainer{" +
                "cognome='" + cognome + '\'' +
                ", età=" + età +
                '}';
    }
}
