package servicesTest;

import Drop.DropDB;
import model.City;
import model.Gym;
import org.junit.Before;
import org.junit.Test;
import services.CityService;
import services.GymService;

import java.util.List;

import static org.junit.Assert.*;


public class GymServiceTest {
    private GymService gymService;
    private CityService cityService;
    private City city = new City(20010, 12000, "Lombardia", "Canegrate");

    @Before
    public void before() {
        gymService = new GymService();
        cityService = new CityService();
        DropDB drop = new DropDB();

        drop.dropDatabase();
        drop.dropDatabase();

        city = cityService.create(city);
    }

    @Test
    public void all() {
        Gym gym = createGym(true, city);
        Gym gym2 = createGym(false, city);

        gym = gymService.create(gym);
        gym2 = gymService.create(gym2);

        List<Gym> gyms = gymService.all();
        assertEquals(gyms.size(), 2);
    }

    @Test
    public void get() {
        Gym gym = createGym(true, city);
        gym = gymService.get(gym.getId());
        assertNotNull(gym);
    }


    @Test
    public void create() {
        Gym gym = createGym(true, city);
        assertNotNull(gym);
    }

    @Test
    public void update() {
        Gym gym = createGym(true, city);
        gym.setOpen24h(false);
        gym = gymService.update(gym);
        Gym newGym = gymService.get(gym.getId());

        assertEquals(newGym.isOpen24h(), false);
    }

    @Test
    public void updateCity() {
        City city2 = new City(20000, 1500, "Lombardia", "Nerviano");
        cityService.create(city2);

        Gym gym = createGym(true, city);

        gym.setCity(city2);
        gym = gymService.update(gym);
        Gym gym2 = gymService.get(gym.getId());
        assertEquals(gym2.getCity().getCap(), city2.getCap());

    }

    @Test
    public void delete() {
        Gym gym = createGym(true, city);
        gymService.delete(gym);
        gym = gymService.get(gym.getId());
        assertNull(gym);
    }

    @Test
    public void deleteGymByCity(){
        Gym gym = createGym(true, city);

        cityService.delete(city);

        gym = gymService.get(gym.getId());

        assertNull(gym);

    }

    public Gym createGym(boolean open, City city){
        Gym gym = new Gym(open, city);
        gym = gymService.create(gym);

        return gym;
    }
}
