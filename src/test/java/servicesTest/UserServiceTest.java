package servicesTest;

import Drop.DropDB;
import model.Gym;
import model.PersonalTrainer;
import model.User;
import org.junit.Before;
import org.junit.Test;
import services.GymService;
import services.PersonalTrainerService;
import services.UserService;

import java.util.List;

import static org.junit.Assert.*;

public class UserServiceTest {

    private UserService userService;
    private GymService gymService;
    private PersonalTrainerService personalTrainerService;

    private Gym gym = new Gym(true, null);
    PersonalTrainer trainer = new PersonalTrainer("Polio", "Maschio", 32, gym);

    @Before
    public void before() {
        userService = new UserService();
        gymService = new GymService();
        personalTrainerService = new PersonalTrainerService();
        DropDB drop = new DropDB();

        drop.dropDatabase();
        drop.dropDatabase();

        gym = gymService.create(gym);
        trainer = personalTrainerService.create(trainer);
    }

    @Test
    public void all() {
        User user = createUser("Federico", "Alessi", 22, gym);
        User user2 = createUser("Luca", "Pagani", 22, gym);
        User user3 = createUser("Riccardo", "Dellavedova", 22, gym);

        List<User> users = userService.all();
        assertEquals(users.size(), 3);
    }

    @Test
    public void get() {
        User user = createUser("Federico", "Alessi", 22, gym);

        user = userService.get(user.getCodiceFiscale());
        assertNotNull(user);
    }


    @Test
    public void create() {
        User user = createUser("Federico", "Alessi", 22, gym);
        assertNotNull(user);
    }

    @Test
    public void update() {
        User user = createUser("Federico", "Alessi", 22, gym);

        user.setEtà(23);
        user = userService.update(user);
        User newUser = userService.get(user.getCodiceFiscale());

        assertEquals(newUser.getEtà(), 23);
    }

    @Test
    public void updateCity() {
        Gym gym2 = new Gym(false, null);
        User user = createUser("Federico", "Alessi", 22, gym);

        gymService.create(gym2);

        user.setPalestraId(gym2);
        user = userService.update(user);
        User user2 = userService.get(user.getCodiceFiscale());
        assertEquals(user2.getPalestraId().getId(), gym2.getId());

    }


    @Test
    public void delete() {
        User user = createUser("Federico", "Alessi", 22, gym);

        userService.delete(user);
        user = userService.get(user.getCodiceFiscale());
        assertNull(user);
    }

    @Test
    public void deleteUserByGym(){
        User user = createUser("Federico", "Alessi", 22, gym);

        gymService.delete(gym);

        user = userService.get(user.getCodiceFiscale());

        assertNull(user);

    }


    @Test
    public void setPersonalTrainerToNullByDelete(){
        User user = createUser("Federico", "Alessi", 22, gym);

        user.setPersonalTrainerId(trainer);

        user = userService.update(user);

        personalTrainerService.delete(trainer);

        user = userService.get(user.getCodiceFiscale());

        assertNull(user.getPersonalTrainerId());


    }

    public User createUser(String nome, String cognome, int età, Gym gym){
        User user = new User(nome, cognome, età, gym);
        user = userService.create(user);

        return user;
    }

}