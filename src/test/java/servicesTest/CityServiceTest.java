package servicesTest;

import Drop.DropDB;
import model.City;
import org.junit.Before;
import org.junit.Test;
import services.CityService;

import java.util.List;

import static org.junit.Assert.*;


public class CityServiceTest {
    private CityService cityService;

    @Before
    public void before() {
        cityService = new CityService();
        DropDB drop = new DropDB();
        drop.dropDatabase();
        drop.dropDatabase();
    }

    @Test
    public void all() {
        City city = createCity(20010, 15000, "Lombardia", "Canegrate");
        City city2 = createCity(20020, 12000, "Lombardia", "Nerviano");
        City city3 = createCity(20030, 12000, "Lombardia", "Parabiago");
        City city4 = createCity(20040, 12000, "Lombardia", "Legnano");


        List<City> cities = cityService.all();
        assertEquals(cities.size(), 4);
    }

    @Test
    public void get() {
        City city = createCity(20010, 15000, "Lombardia", "Canegrate");
        city = cityService.get(city.getCap());
        assertNotNull(city.getCap());
        assertNotNull(city.getRegione());
        assertNotNull(city.getAbitanti());
        assertNotNull(city.getNome());

    }




    @Test
    public void create() {
        City city = createCity(20010, 15000, "Lombardia", "Canegrate");
        assertNotNull(city);
    }

    @Test
    public void update() {
        City city = createCity(20010, 15000, "Lombardia", "Canegrate");
        city.setRegione("Lazio");
        city = cityService.update(city);
        City newCity = cityService.get(city.getCap());

        assertEquals(newCity.getRegione(), "Lazio");
    }

    @Test
    public void delete() {
        City city = createCity(20010, 15000, "Lombardia", "Canegrate");
        cityService.delete(city);
        city = cityService.get(city.getCap());
        assertNull(city);
    }


    public City createCity(int cap, int abitanti, String regione, String nome){
        City city = new City(cap, abitanti, regione, nome);
        city = cityService.create(city);

        return city;
    }









}
