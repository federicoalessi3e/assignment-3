package servicesTest;

import Drop.DropDB;
import model.Gym;
import model.PersonalTrainer;
import org.junit.Before;
import org.junit.Test;
import services.GymService;
import services.PersonalTrainerService;

import java.util.List;

import static org.junit.Assert.*;

public class PersonalTrainerServiceTest {
    private GymService gymService;
    private PersonalTrainerService personalTrainerService;
    private Gym gym = new Gym(true, null);

    @Before
    public void before() {
        gymService = new GymService();
        personalTrainerService = new PersonalTrainerService();
        DropDB drop = new DropDB();

        drop.dropDatabase();
        drop.dropDatabase();

        gym = gymService.create(gym);
    }

    @Test
    public void all() {
        //Trainers
        PersonalTrainer trainer = createTrainer("Polio", "Maschio", 32, gym);
        PersonalTrainer trainer2 = createTrainer("Nudo", "Maschio", 21, gym);
        PersonalTrainer trainer3 = createTrainer("Zini", "Femmina", 25, gym);


        List<PersonalTrainer> trainers = personalTrainerService.all();
        assertEquals(trainers.size(), 3);
    }

    @Test
    public void get() {
        PersonalTrainer trainer = createTrainer("Polio", "Maschio", 32, gym);
        trainer = personalTrainerService.get(trainer.getId());
        assertNotNull(trainer);
    }


    @Test
    public void create() {
        PersonalTrainer trainer = createTrainer("Polio", "Maschio", 32, gym);
        assertNotNull(trainer);
    }

    @Test
    public void update() {
        PersonalTrainer trainer = createTrainer("Polio", "Maschio", 32, gym);
        trainer.setEtà(42);
        trainer = personalTrainerService.update(trainer);
        PersonalTrainer newTrainer = personalTrainerService.get(trainer.getId());

        assertEquals(newTrainer.getEtà(), 42);
    }

    @Test
    public void updateCity() {
        Gym gym2 = new Gym(false, null);
        gymService.create(gym2);

        PersonalTrainer trainer = createTrainer("Polio", "Maschio", 32, gym);

        trainer.setPalestraId(gym2);
        trainer = personalTrainerService.update(trainer);
        PersonalTrainer trainer2 = personalTrainerService.get(trainer.getId());
        assertEquals(trainer.getPalestraId().getId(), gym2.getId());

    }

    @Test
    public void delete() {
        PersonalTrainer trainer = createTrainer("Polio", "Maschio", 32, gym);
        personalTrainerService.delete(trainer);
        trainer = personalTrainerService.get(trainer.getId());
        assertNull(trainer);
    }

    @Test
    public void deleteTrainerByGym(){
        PersonalTrainer trainer = createTrainer("Polio", "Maschio", 32, gym);

        gymService.delete(gym);

        trainer = personalTrainerService.get(trainer.getId());

        assertNull(trainer);

    }

    public PersonalTrainer createTrainer(String cognome, String sesso, int età, Gym gym){
        PersonalTrainer trainer = new PersonalTrainer(cognome, sesso, età, gym);
        trainer = personalTrainerService.create(trainer);

        return trainer;
    }
}